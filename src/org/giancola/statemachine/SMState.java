package org.giancola.statemachine;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class SMState implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String m_name;
    private final ArrayList<SMAction> m_entryActions = new ArrayList<SMAction>();
    private final ArrayList<SMAction> m_exitActions = new ArrayList<SMAction>();
    private final ArrayList<SMAction> m_preActions = new ArrayList<SMAction>();
    private final ArrayList<SMAction> m_postActions = new ArrayList<SMAction>();
    private final ArrayList<SMTrans> m_overrideTransList = new ArrayList<SMTrans>();
    private final ArrayList<SMTrans> m_transList = new ArrayList<SMTrans>();
    private SMTrans m_defaultTrans = null;

    public SMState()
    {
        super();
        m_name = new String("<noName>");
    }

    public SMState(String name)
    {
        super();
        this.m_name = name;
    }

    @Override
    public String toString()
    {
        String diagram = new String();
        String indent = "    ";

        // need to put in ID somewhere.
        diagram += getName() + "\n";
        diagram += "{\n";

        if (m_entryActions.size() > 0)
        {
            diagram += indent + "Entry:\n";
            for (SMAction action : m_entryActions)
            {
                diagram += indent + indent + action.getAction() + "\n";
            }
        }

        if (m_exitActions.size() > 0)
        {
            diagram += indent + "Exit:\n";
           for (SMAction action : m_exitActions)
            {
                diagram += indent + indent + action.getAction() + "\n";
            }
        }

        if (m_overrideTransList.size() > 0)
        {
            diagram += indent + "Override:\n";
            for (SMTrans trans : m_overrideTransList)
            {
                diagram += StateDiagram.indentLines(trans.toString(), indent + indent);
            }
        }

        if (m_transList.size() > 0)
        {
            diagram += indent + "Transitions:\n";
            for (SMTrans trans : m_transList)
            {
                diagram += StateDiagram.indentLines(trans.toString(), indent + indent);
            }
        }

        if (m_defaultTrans != null)
        {
            diagram += indent + "Default:\n";
            diagram += StateDiagram.indentLines(m_defaultTrans.toString(), indent + indent);
        }

        diagram += "}\n";
        return diagram;
//pag/        state += "\n-------> State: " + getName() + "\n";
//pag/        state += "Entry Actions:\n";
//pag/        for (SMAction action : m_entryActions)
//pag/        {
//pag/            state += "    " + action.toString() + "\n";
//pag/        }
//pag/        state += "Exit Actions:\n";
//pag/        for (SMAction action : m_exitActions)
//pag/        {
//pag/            state += "    " + action.toString() + "\n";
//pag/        }
//pag/        state += "Pre-Actions:\n";
//pag/        for (SMAction action : m_preActions)
//pag/        {
//pag/            state += "    " + action.toString() + "\n";
//pag/        }
//pag/        state += "Post-Actions:\n";
//pag/        for (SMAction action : m_postActions)
//pag/        {
//pag/            state += "    " + action.toString() + "\n";
//pag/        }
//pag/        state += "Override Transitions:\n";
//pag/        for (SMTrans trans : m_overrideTransList)
//pag/        {
//pag/            state += trans.toString();
//pag/        }
//pag/        state += "Transitions:\n";
//pag/        for (SMTrans trans : m_transList)
//pag/        {
//pag/            state += trans.toString();
//pag/        }
//pag/        state += "Default Transisiton: ";
//pag/        if (m_defaultTrans != null)
//pag/        {
//pag/            state += m_defaultTrans.toString();
//pag/        }
//pag/        else
//pag/        {
//pag/            state += "none\n";
//pag/        }
//pag/
//pag/        return state;
    }

    public String getName()
    {
        return m_name;
    }

    public void setName(String name)
    {
        m_name = name;
    }

    public void addTrans(SMTrans tr)
    {
        m_transList.add(tr);
    }

    public void addOverrideTrans(SMTrans tr)
    {
        m_overrideTransList.add(tr);
    }

    public void setDefaultTrans(SMTrans tr)
    {
        m_defaultTrans = tr;
    }

    public SMTrans getDefaultTrans()
    {
        return m_defaultTrans;
    }

    public ArrayList<SMTrans> getTransitions()
    {
        return m_transList;
    }

    // Returns the first transition that matches.
    // Returns null if no transition matches and there is no default.
    public SMTrans execute(SMMatch input)
    {
        // First, search the override transition list.
        for (SMTrans tr : m_overrideTransList)
        {
            if (tr.isMatch(input))
            {
                return tr;
            }
        }

        // Next, search the regular transition list.
        for (SMTrans tr : m_transList)
        {
            if (tr.isMatch(input))
            {
                return tr;
            }
        }

        // Finally, if nothing else matched, take the default transition,
        // if any.
        if (null != m_defaultTrans)
        {
            return m_defaultTrans;
        }
        return null;
    }

     // A shortcut to create GOTO transitions.
    public Boolean buildTrans(SMMatch match, SMState endState,
            ArrayList<SMAction> actions)
    {
        if (findTrans(match))
        {
            return false;
        }
        SMTrans tr = new SMTrans(match, endState);
        for (SMAction action : actions)
        {
            tr.addAction(action);
        }
        addTrans(tr);
        return true;
    }

     // Checks for a matching transition in the regular transition list.
    public boolean findTrans(SMMatch match)
    {
        for (SMTrans trans : m_transList)
        {
            if (trans.isMatch(match))
            {
                return true;
            }
        }
        return false;
    }

     // Checks for a matching transition in the override transition list.
    public boolean findOverrideTrans(SMMatch match)
    {
        for (SMTrans trans : m_overrideTransList)
        {
            if (trans.isMatch(match))
            {
                return true;
            }
        }
        return false;
    }

    public ArrayList<SMAction> getEntryActions()
    {
        return m_entryActions;
    }

    public ArrayList<SMAction> getExitActions()
    {
        return m_exitActions;
    }
    public ArrayList<SMAction> getPreActions()
    {
        return m_preActions;
    }

    public ArrayList<SMAction> getPostActions()
    {
        return m_postActions;
    }

    public void addEntryAction(SMAction action)
    {
        m_entryActions.add(action);
    }

    public void addExitAction(SMAction action)
    {
        m_exitActions.add(action);
    }
    public void addPreAction(SMAction action)
    {
        m_preActions.add(action);
    }

    public void addPostAction(SMAction action)
    {
        m_postActions.add(action);
    }

    public void toXml(XMLStreamWriter writer) throws XMLStreamException
    {
        writer.writeCharacters("\n");
        writer.writeComment(" ====== State ====== ");
        writer.writeCharacters("\n");
        writer.writeStartElement(StateDiagram.STATE);
        writer.writeAttribute(StateDiagram.NAME, m_name);
        writer.writeCharacters("\n");

        if (m_entryActions.size() > 0)
        {
            writer.writeComment("Entry Actions");
            writer.writeCharacters("\n");
            writer.writeStartElement(StateDiagram.ENTRYACTIONS);
            writer.writeCharacters("\n");
            for (SMAction action : m_entryActions)
            {
                action.toXml(writer);
                writer.writeCharacters("\n");
            }
            writer.writeEndElement();
            writer.writeCharacters("\n");
        }

        if (m_exitActions.size() > 0)
        {
            writer.writeComment("Exit Actions");
            writer.writeCharacters("\n");
            writer.writeStartElement(StateDiagram.EXITACTIONS);
            writer.writeCharacters("\n");
            for (SMAction action : m_exitActions)
            {
                action.toXml(writer);
                writer.writeCharacters("\n");
            }
            writer.writeEndElement();
            writer.writeCharacters("\n");
        }

        if (m_preActions.size() > 0)
        {
            writer.writeComment("Pre-Actions");
            writer.writeCharacters("\n");
            writer.writeStartElement(StateDiagram.PREACTIONS);
            writer.writeCharacters("\n");
            for (SMAction action : m_preActions)
            {
                action.toXml(writer);
                writer.writeCharacters("\n");
            }
            writer.writeEndElement();
            writer.writeCharacters("\n");
        }

        if (m_postActions.size() > 0)
        {
            writer.writeComment("Post-Actions");
            writer.writeCharacters("\n");
            writer.writeStartElement(StateDiagram.POSTACTIONS);
            writer.writeCharacters("\n");
            for (SMAction action : m_postActions)
            {
                action.toXml(writer);
                writer.writeCharacters("\n");
            }
            writer.writeEndElement();
            writer.writeCharacters("\n");
        }

        if (m_overrideTransList.size() > 0)
        {
            writer.writeComment("Override Transition List");
            writer.writeCharacters("\n");
            writer.writeStartElement(StateDiagram.OVERRIDE);
            writer.writeCharacters("\n");
            for (SMTrans trans : m_overrideTransList)
            {
                trans.toXml(writer);
            }
            writer.writeEndElement();
            writer.writeCharacters("\n");
        }

        if (m_transList.size() > 0)
        {
            writer.writeComment("Transition List");
            writer.writeCharacters("\n");
            writer.writeStartElement(StateDiagram.TRANSITIONS);
            writer.writeCharacters("\n");
            for (SMTrans trans : m_transList)
            {
                trans.toXml(writer);
            }
            writer.writeEndElement();
            writer.writeCharacters("\n");
        }

        if (m_defaultTrans != null)
        {
            writer.writeComment("Default Transition");
            writer.writeCharacters("\n");
            writer.writeStartElement(StateDiagram.DEFAULT);
            writer.writeCharacters("\n");
            m_defaultTrans.toXml(writer);
            writer.writeEndElement();
            writer.writeCharacters("\n");
        }
        writer.writeEndElement();
        writer.writeCharacters("\n");
    }

//pag/    public String printSource(String indent)
//pag/    {
//pag/        String diagram = new String();
//pag/        String newIndent = new String();
//pag/        String indentSize = "    ";
//pag/diagram += "Enter State::printSource()\n";
//pag/
//pag/        // need to put in ID somewhere.
//pag/        diagram += indent + getName() + "\n";
//pag/        diagram += indent + "{\n";
//pag/
//pag/        newIndent += indent + indent;
//pag/        diagram += newIndent + "Entry:\n";
//pag/        for (SMAction action : m_entryActions)
//pag/        {
//pag/            diagram += newIndent + indent + action.getAction() + "\n";
//pag/        }
//pag/        diagram += newIndent + "Exit:\n";
//pag/        for (SMAction action : m_exitActions)
//pag/        {
//pag/            diagram += newIndent + indent + action.getAction() + "\n";
//pag/        }
//pag/
//pag/        diagram += newIndent + "Override:\n";
//pag/        for (SMTrans trans : m_overrideTransList)
//pag/        {
//pag/            diagram += StateDiagram.indentLines(trans.toString(), newIndent + indent);
//pag/        }
//pag/
//pag/        diagram += newIndent + "Transitions:\n";
//pag/        for (SMTrans trans : m_transList)
//pag/        {
//pag/diagram += "Calling Trans::printSource()\n";
//pag/            diagram += StateDiagram.indentLines(trans.toString(), newIndent + indent);
//pag/diagram += "After Trans::printSource()\n";
//pag/        }
//pag/
//pag/        if (m_defaultTrans != null)
//pag/        {
//pag/            diagram += newIndent + "Default:\n";
//pag/            diagram += StateDiagram.indentLines(m_defaultTrans.toString(), indent);
//pag/        }
//pag/
//pag/        diagram += indent + "}\n";
//pag/diagram += "Leave State::printSource()\n";
//pag/        return diagram;
//pag/    }

    public ArrayList<SMMatch> getAllMatches()
    {
        ArrayList<SMMatch> matches = new ArrayList<SMMatch>();

        for (SMTrans trans : m_overrideTransList)
        {
            matches.add(trans.getMatch());
        }
        for (SMTrans trans : m_transList)
        {
            matches.add(trans.getMatch());
        }
        return matches;
    }

    public ArrayList<SMAction> getAllActions()
    {
        ArrayList<SMAction> actions = new ArrayList<SMAction>();

        if (m_defaultTrans != null)
        {
            actions.addAll(m_defaultTrans.getActions());
        }
        actions.addAll((m_entryActions));
        actions.addAll(m_exitActions);
        for (SMTrans trans : m_overrideTransList)
        {
            actions.addAll(trans.getActions());
        }
        for (SMTrans trans : m_transList)
        {
            actions.addAll(trans.getActions());
        }
        return actions;
    }
}
