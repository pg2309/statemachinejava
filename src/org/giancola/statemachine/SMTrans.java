package org.giancola.statemachine;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.giancola.statemachine.SMNextState.TransType;

public class SMTrans implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    protected SMMatch m_match = null;
    protected SMNextState m_nextState = null;
    protected String m_nextStateStr = null;
    protected TransType m_transType = null;
    private final ArrayList<SMAction> m_actionList = new ArrayList<SMAction>();

    /**
     * This constructor is for an unmatched LOOPBACK type transitions.
     */
    public SMTrans()
    {
        super();
        this.m_match = null;
        this.m_nextState = new SMNextState(TransType.LOOPBACK);
    }

     // This constructor is for LOOPBACK type transitions.
    public SMTrans(SMMatch match)
    {
        super();
        this.m_match = match;
        this.m_nextState = new SMNextState(TransType.LOOPBACK);
    }

     // This constructor is for LOOPBACK or RETURN type transitions.
    public SMTrans(SMMatch match, TransType type)
    {
        super();
        this.m_match = match;
        this.m_nextState = new SMNextState(type);
    }

     // This constructor is for NORMAL type transitions.
    public SMTrans(SMMatch match, SMState endState)
    {
        super();
        this.m_match = match;
        this.m_nextState = new SMNextState(endState);
    }

     // This constructor is for any type transitions.
    public SMTrans(SMMatch match, SMState endState, TransType type)
    {
        super();
        this.m_match = match;
        this.m_nextState = new SMNextState(endState, type);
    }

     // This constructor is for any type transitions. It keeps the
    public SMTrans(SMMatch match, String endState, TransType type)
    {
        super();
        this.m_match = match;
        this.m_nextStateStr = endState;
        this.m_transType = type;
        this.m_nextState = null;
    }

     // This constructor is used as a GOTO transition that will match anything.
    public SMTrans(SMState endState)
    {
        super();
        this.m_match = null;
        this.m_nextState = new SMNextState(endState);
    }

    public String toString()
    {
        String diagram = new String();
        String indent = "    ";

        try
        {
            // Print the goto state.
            SMMatch mt = getMatch();
            if (null == mt)
            {
                diagram += "<any>";
            }
            else
            {
                diagram += "On: " + mt.getKey();
            }

            // We may not have a SMNextState object yet, so use the
            // m_transType if present.
            TransType type = TransType.GOTO;
            if (m_transType == null)
            {
                type = m_nextState.getType();
            }
            else
            {
                type = m_transType;
            }
            switch (type)
            {
            case GOTO:
                diagram += ", goto " + m_nextState.getNext().getName();
                break;
            case CALL:
                diagram += ", call " + m_nextState.getNext().getName();
                break;
            case RETURN:
                diagram += ", return";
                break;
            case LOOPBACK:
                diagram += ", loopback";
                break;
            case NONE:
                break;
            default:
                break;                
            }
            diagram += "\n";
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for (SMAction action : getActions())
        {
            diagram += indent + action.toString() + "\n";
        }
        return diagram;
    }

    protected String toStringActionList()
    {
        String trans = new String();

        trans += "Actions:\n";
        for (SMAction action : m_actionList)
        {
            trans += "    " + action.toString() + "\n";
        }
        return trans;
    }

    public void addAction(SMAction ac)
    {
        m_actionList.add(ac);
    }
    
    public Boolean needsStateResolved()
    {
        if ((m_nextState == null) && (m_nextStateStr != null))
        {
            return true;
        }
        return false;
    }
    
    public String getNextStateStr()
    {
        return m_nextStateStr;
    }
    
    public TransType getTransType()
    {
        if (m_transType == null)
        {
            return m_nextState.getType();
        }
        else
        {
            return m_transType;
        }
    }

    public Boolean isMatch(SMMatch input)
    {
        if (m_match == null) return true;
        if (m_match.testMatch(input)) return true;
        return false;
    }

    public ArrayList<SMAction> getActions()
    {
        return m_actionList;
    }

    public SMMatch getMatch()
    {
        return m_match;
    }

    public SMNextState getNextState()
    {
        return m_nextState;
    }
    
    public void setNextState(SMNextState nextState)
    {
        m_nextState = nextState;
        m_nextStateStr = null;
        m_transType = null;
    }

    public void toXml(XMLStreamWriter writer) throws XMLStreamException
    {
        writer.writeStartElement(StateDiagram.TRANSITION);
        if ((null != m_match) && (null != m_match.getKey()))
        {
            writer.writeAttribute(StateDiagram.MATCH, m_match.getKey());
        }
        if (null != m_nextState)
        {
            writer.writeAttribute(StateDiagram.TYPE, m_nextState.getStringType());
            if ((m_nextState.getType() == TransType.CALL)
                || (m_nextState.getType() == TransType.GOTO))
            {
                writer.writeAttribute(StateDiagram.STATE, m_nextState.getNext().getName());
            }
        }
        writer.writeCharacters("\n");

        toXmlActionList(writer);
        writer.writeEndElement();
        writer.writeCharacters("\n");
    }

    protected void toXmlActionList(XMLStreamWriter writer)
            throws XMLStreamException
    {
        for (SMAction action : m_actionList)
        {
            action.toXml(writer);
            writer.writeCharacters("\n");
        }
    }
}

/*
 * void CSMTransition::ParseMatch() { list<CMMMatch *>::iterator im;
 * stack<CMMMatch *> tempStack; CMMMatch *temp, *curr; int type, prevType, size;
 * 
 * // Fake "And" operator used to fill in missing operators static CMMMatch
 * tempMatchAnd(CMMMatch::MatchType_And);
 * 
 * m_parsedMatchList.clear(); size = m_matchList.size(); if (size > 0) { if
 * (size == 1) { im = m_matchList.begin();
 * m_parsedMatchList.insert(m_parsedMatchList.end(), *im); } else {
 * 
 * // There is more than one match in the list // Convert the infix match list
 * to postfix to make // it easier to execute prevType =
 * CMMMatch::MatchType_None; for (im = m_matchList.begin(); im !=
 * m_matchList.end(); im++) { curr = *im; type = curr->GetMatchType();
 * 
 * // If there are two value matches in a row, fill in // the missing "And"
 * operator if ((prevType == CMMMatch::MatchType_Value) && ((type ==
 * CMMMatch::MatchType_Value) || (type == CMMMatch::MatchType_Not))) { if
 * (tempStack.empty()) { tempStack.push(&tempMatchAnd); } else { if
 * (tempStack.top()->GetMatchType() == CMMMatch::MatchType_Begin) {
 * tempStack.push(&tempMatchAnd); } else { if (type ==
 * CMMMatch::MatchType_Value) { while (!tempStack.empty() &&
 * ((tempStack.top()->GetMatchType() == CMMMatch::MatchType_Not) ||
 * (tempStack.top()->GetMatchType() == CMMMatch::MatchType_And))) { temp =
 * tempStack.top(); m_parsedMatchList.insert(m_parsedMatchList.end(), temp);
 * tempStack.pop(); } } else { while (!tempStack.empty() &&
 * (tempStack.top()->GetMatchType() == CMMMatch::MatchType_Not)) { temp =
 * tempStack.top(); m_parsedMatchList.insert(m_parsedMatchList.end(), temp);
 * tempStack.pop(); } } tempStack.push(&tempMatchAnd); } } }
 * 
 * // Process the current match switch (type) {
 * 
 * // Values just get added to the output list case CMMMatch::MatchType_Value:
 * m_parsedMatchList.insert(m_parsedMatchList.end(), curr); break;
 * 
 * // "And" operators have precedence over "Or" operators case
 * CMMMatch::MatchType_And: if (tempStack.empty()) { tempStack.push(curr); }
 * else { if (tempStack.top()->GetMatchType() == CMMMatch::MatchType_Begin) {
 * tempStack.push(curr); } else { while (!tempStack.empty() &&
 * ((tempStack.top()->GetMatchType() == CMMMatch::MatchType_Not) ||
 * (tempStack.top()->GetMatchType() == CMMMatch::MatchType_And))) { temp =
 * tempStack.top(); m_parsedMatchList.insert(m_parsedMatchList.end(), temp);
 * tempStack.pop(); } tempStack.push(curr); } } break;
 * 
 * // "Or" operators are the lowest precedence case CMMMatch::MatchType_Or: if
 * (tempStack.empty()) { tempStack.push(curr); } else { if
 * (tempStack.top()->GetMatchType() == CMMMatch::MatchType_Begin) {
 * tempStack.push(curr); } else { while (!tempStack.empty() &&
 * ((tempStack.top()->GetMatchType() == CMMMatch::MatchType_Not) ||
 * (tempStack.top()->GetMatchType() == CMMMatch::MatchType_And) ||
 * (tempStack.top()->GetMatchType() == CMMMatch::MatchType_Or))) { temp =
 * tempStack.top(); m_parsedMatchList.insert(m_parsedMatchList.end(), temp);
 * tempStack.pop(); } tempStack.push(curr); } } break;
 * 
 * // "Not" operators have precedence over all others, just // add them to the
 * output list case CMMMatch::MatchType_Not: tempStack.push(curr); break;
 * 
 * // Add "Begin" operators to the output list case CMMMatch::MatchType_Begin:
 * tempStack.push(curr); break;
 * 
 * // Match up an "End" operator with the most recent "Begin" operator //
 * copying everything between them to the output list case
 * CMMMatch::MatchType_End: while (!tempStack.empty() &&
 * (tempStack.top()->GetMatchType() != CMMMatch::MatchType_Begin)) { temp =
 * tempStack.top(); m_parsedMatchList.insert(m_parsedMatchList.end(), temp);
 * tempStack.pop(); } if (!tempStack.empty() && (tempStack.top()->GetMatchType()
 * == CMMMatch::MatchType_Begin)) { tempStack.pop(); } break; } prevType = type;
 * }
 * 
 * // Copy anything left on the stack to the output list while
 * (!tempStack.empty()) { temp = tempStack.top();
 * m_parsedMatchList.insert(m_parsedMatchList.end(), temp); tempStack.pop(); } }
 * } }
 */
