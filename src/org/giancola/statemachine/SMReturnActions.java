package org.giancola.statemachine;

import java.util.ArrayList;

public class SMReturnActions
{
    private SMNextState m_nextState = null;
    private ArrayList<SMAction> m_actions = null;

    public SMReturnActions()
    {
    }

    public SMNextState getNextState()
    {
        return m_nextState;
    }

    public void setNextState(SMNextState state)
    {
        this.m_nextState = state;
    }

    public ArrayList<SMAction> getActions()
    {
        return m_actions;
    }

    public void setActions(ArrayList<SMAction> actions)
    {
        this.m_actions = actions;
    }

    public SMReturnActions(SMNextState nextState, ArrayList<SMAction> actions)
    {
        super();
        this.m_nextState = nextState;
        this.m_actions = actions;
    }

}
