package org.giancola.statemachine;

import java.io.Serializable;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class SMAction implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public enum ActionReturn
    {
        NORMAL, TERMINATE, VETO, STOPACTIONS, STOPACTIONSALL
    }

    private String m_action;

    public SMAction()
    {
        m_action = new String();
    }

    public SMAction(String action)
    {
        m_action = action;
    }

    public String toString()
    {
        String action = new String();

        action += StateDiagram.ACTION;
        action += ": " + m_action;
        return action;
    }

    public String getAction()
    {
        return m_action;
    }

    public void setAction(String action)
    {
        m_action = action;
    }

    public void toXml(XMLStreamWriter writer) throws XMLStreamException
    {
        writer.writeEmptyElement(StateDiagram.ACTION);
        writer.writeAttribute(StateDiagram.NAME, m_action);
    }

}
