package org.giancola.statemachine;

import java.io.Serializable;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class SMNextState implements Serializable
{
    // ToDo: Do we still need SMNextState?

    private static final long serialVersionUID = 1L;
    private SMState m_next = null;
    private TransType m_type = TransType.GOTO;

    public enum TransType
    {
        GOTO, CALL, RETURN, LOOPBACK, NONE
    }

    public SMNextState(SMState next)
    {
        super();
        this.m_next = next;
    }

    public SMNextState(TransType type)
    {
        super();
//pag/        if ((type != TransType.LOOPBACK) && (type != TransType.RETURN))
//pag/        {
//pag/            // Throw something. But for now just force it to be a loopback.
//pag/            type = TransType.LOOPBACK;
//pag/        }
        this.m_type = type;
        this.m_next = null;
    }

    public SMNextState(SMState next, TransType type)
    {
        super();
        this.m_next = next;
        this.m_type = type;
    }

    public String getStringType()
    {
        switch (m_type)
        {
        case GOTO:
            return "goto";
        case CALL:
            return "call";
        case RETURN:
            return "return";
        case LOOPBACK:
            return "loopback";
        case NONE:
            return "none";
        default:
            return "unknown";
        }
    }

    public String toString()
    {
        String out = new String();

        switch (m_type)
        {
        case GOTO:
            out += "Trans to " + m_next.getName();
            break;
        case CALL:
            out += "Call to " + m_next.getName();
            break;
        case RETURN:
            out += "Return";
            break;
        case LOOPBACK:
            out += "Loopback";
            break;
        case NONE:
            break;
        default:
            out += "Unknown Type";
            break;
        }
        return out;
    }

    public void toXml(XMLStreamWriter writer) throws XMLStreamException
    {
        writer.writeStartElement("transtype");
        switch (m_type)
        {
        case GOTO:
            writer.writeAttribute("type", "goto");
            writer.writeCharacters(m_next.getName());
            break;
        case CALL:
            writer.writeAttribute("type", "call");
            writer.writeCharacters(m_next.getName());
            break;
        case RETURN:
            writer.writeAttribute("type", "return");
            break;
        case LOOPBACK:
            writer.writeAttribute("type", "loopback");
            break;
        case NONE:
            writer.writeAttribute("type", "none");
            break;
        }
        writer.writeEndElement();
    }

    public SMState getNext()
    {
        return m_next;
    }

    public void setNext(SMState next)
    {
        this.m_next = next;
    }

    public TransType getType()
    {
        return m_type;
    }

    public void setType(TransType type)
    {
        this.m_type = type;
    }
}
