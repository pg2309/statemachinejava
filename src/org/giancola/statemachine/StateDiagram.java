package org.giancola.statemachine;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.giancola.statemachine.SMNextState.TransType;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;


/*
 * Todo: 
 * = Make the matches and actions singular. Keep a global list of unique 
 * names and just use references in the transitions.
 * = Or, make the XML reader take match and action declarations with an ID 
 * and then use the IDs in the transitions.
 * = The match and action declarations could just be lists of strings and
 * then the IDs would be calculated from the strings.
 * = Need to add the pre and post action lists, global and per state.
 * = Should make the global transitions be just the list names, remove
 * the need for the "Global" prefix.
 * 
 * 
 * 
 * Possible new text state diagram format:
 * 
 *    new_state = curr_state + match / action, action, action;
 *    state_a:: enter/ action, action, action exit/ action, action;
 * How to do loopback?
 * How to do call/return?
 * How to do global?
 * How to do defaults?
 * 
 * 
 * Transition counters
 *   total
 *   running average across the state
 *   running %
 * Log of states passed through
 * Return path to a given state from the current state (or another)
 * Return list of next trans from current state (or another)
 *   need to indicate state trans vs global vs default vs global default
 * 
 */
public class StateDiagram extends DefaultHandler implements Serializable
{
    private static final long serialVersionUID = 1L;
    public static final String ACTION = "action";
    public static final String DEFAULT = "default";
    public static final String ENTRYACTIONS = "entryactions";
    public static final String EXITACTIONS = "exitactions";
    public static final String GLOBALDEFAULTTRANS = "globaldefaulttrans";
    public static final String GLOBALOVERRIDETRANS = "globaloverridetrans";
    public static final String GLOBALPOSTACTION = "globalpostaction";
    public static final String GLOBALPOSTACTIONS = "globalpostactions";
    public static final String GLOBALPOSTMATCHACTIONS = "globalpostmatchactions";
    public static final String GLOBALPREACTIONS = "globalpreactions";
    public static final String GLOBALPREMATCHACTIONS = "globalprematchactions";
    public static final String GLOBALTRANS = "globaltrans";
    public static final String KEY = "key";
    public static final String MATCH = "match";
    public static final String NAME = "name";
    public static final String OVERRIDE = "override";
    public static final String POSTACTIONS = "postactions";
    public static final String PREACTIONS = "preactions";
    public static final String START = "Start";
    public static final String STARTSTATE = "startstate";
    public static final String STATE = "state";
    public static final String STATEDIAGRAM = "statediagram";
    public static final String TRANSITION = "transition";
    public static final String TRANSITIONS = "transitions";
    public static final String TYPE = "type";
    private final ArrayList<SMState> m_stateList = new ArrayList<SMState>();
    private SMState m_startState;
    private String m_name = new String();
    private final SMState m_globalOverrideState = new SMState("<globalOverride>");
    private final SMState m_globalState = new SMState("<global>");
    private final SMState m_globalDefaultState = new SMState("<globalDefault>");

    // Global pre and post actions are executed before and after all other
    // actions. These do not perform a match on the input.
    private final SMTrans m_globalPreActions = new SMTrans();
    private final SMTrans m_globalPostActions = new SMTrans();

    // Global pre and post match actions are executed after the global pre
    // and before the global post actions. These global match actions
    // only execute when there is a matching input.
    private final ArrayList<SMTrans> m_globalPreMatchActionList = new ArrayList<SMTrans>();
    private final ArrayList<SMTrans> m_globalPostMatchActionList = new ArrayList<SMTrans>();

    public StateDiagram()
    {
        super();
        m_name = "<no name>";
    }

    public StateDiagram(String name)
    {
        super();
        m_name = name;
    }

    public String getName()
    {
        return m_name;
    }

    public void addState(SMState st)
    {
        m_stateList.add(st);
    }

    public void setStartState(SMState st)
    {
        if (m_stateList.indexOf(st) != -1)
        {
            m_startState = st;
        }
    }

    public SMState getStartState()
    {
        return m_startState;
    }

    public void addGlobalPreAction(SMAction action)
    {
        m_globalPreActions.addAction(action);
    }

    public void addGlobalPreMatchAction(SMTrans trans)
    {
        // Add post match actions from a transition.
        SMMatch match = trans.getMatch();
        for (SMAction action : trans.getActions())
        {
            addGlobalPreMatchAction(match, action);
        }
    }

    public void addGlobalPreMatchAction(SMMatch match, SMAction action)
    {
        boolean found = false;

        for (SMTrans tr : m_globalPreMatchActionList)
        {
            if (tr.isMatch(match))
            {
                // Found the matching transition, add the new action.
                tr.addAction(action);
                found = true;
                break;
            }
        }

        // If we didn't find an matching transition, add one.
        if (!found)
        {
            SMTrans trans = new SMTrans(match, TransType.NONE);
            trans.addAction(action);
            m_globalPreMatchActionList.add(trans);
        }
    }

    public void addGlobalPostMatchAction(SMTrans trans)
    {
        // Add post match actions from a transition.
        SMMatch match = trans.getMatch();
        for (SMAction action : trans.getActions())
        {
            addGlobalPostMatchAction(match, action);
        }
    }

    public void addGlobalPostMatchAction(SMMatch match, SMAction action)
    {
        boolean found = false;

        for (SMTrans tr : m_globalPostMatchActionList)
        {
            if (tr.isMatch(match))
            {
                // Found the matching transition, add the new action.
                tr.addAction(action);
                found = true;
                break;
            }
        }

        // If we didn't find matching transition, add one.
        if (!found)
        {
            SMTrans trans = new SMTrans(match, TransType.NONE);
            trans.addAction(action);
            m_globalPostMatchActionList.add(trans);
        }
    }

    public void addGlobalPostAction(SMAction action)
    {
        m_globalPostActions.addAction(action);
    }

    public void addGlobalOverrideTrans(SMTrans tr)
    {
        m_globalOverrideState.addTrans(tr);
    }

    public void addGlobalTrans(SMTrans tr)
    {
        m_globalState.addTrans(tr);
    }

    public void setGlobalDefaultTrans(SMTrans tr)
    {
        m_globalDefaultState.setDefaultTrans(tr);
    }
    
    // Find a state by name.
    public SMState findState(String name)
    {
        for (SMState state : m_stateList)
        {
            if (state.getName().equals(name))
            {
                return state;
            }
        }
        return null;
    }

    // Indent the lines by the given amount.
    public static String indentLines(String lines, String indent)
    {
        StringBuilder indented = new StringBuilder(new String());
        String remain = lines;
        while (remain != null)
        {
            int ind = remain.indexOf("\n");
            if (ind > -1)
            {
                indented.append(indent).append(remain.substring(0, ind));
                remain = remain.substring(1 + ind);
                indented.append("\n");
            }
            else
            {
                indented.append(remain);
                remain = null;
            }
        }
        return indented.toString();
    }

    @Override
    public String toString()
    {
        StringBuilder diagram = new StringBuilder(new String());
        String indent = new String();
        String indentSize = new String("    ");

        diagram.append(m_name).append("\n");
        diagram.append("{\n");
        indent += indentSize;
        diagram.append(indent).append(START).append(": ").append(m_startState.getName()).append("\n");
        diagram.append("\n");

        // Need to print global transitions and actions.
        // Write the global override transitions.
        ArrayList<SMTrans> transList = m_globalOverrideState.getTransitions();
        if (transList.size() > 0)
        {
            diagram.append(indent).append(GLOBALOVERRIDETRANS).append("\n");
            diagram.append(indent).append("{\n");
            for (SMTrans trans : transList)
            {
                diagram.append(indentLines(trans.toString(), indentSize + indent));
            }
            diagram.append(indent).append("}\n\n");
        }

        // Write the global transitions.
        transList = m_globalState.getTransitions();
        if (transList.size() > 0)
        {
            diagram.append(indent).append(GLOBALTRANS).append("\n");
            diagram.append(indent).append("{\n");
            for (SMTrans trans : transList)
            {
                diagram.append(indentLines(trans.toString(), indentSize + indent));
            }
            diagram.append(indent).append("}\n\n");
        }

        // Write the global default transition.
        SMTrans trans = m_globalDefaultState.getDefaultTrans();
        if (trans != null)
        {
            diagram.append(indent).append(GLOBALDEFAULTTRANS).append("\n");
            diagram.append(indentLines(trans.toString(), indentSize + indent));
            diagram.append("\n\n");
        }

        // Write the global pre-actions.
        diagram.append(indent).append(GLOBALPREACTIONS).append("\n");
        diagram.append(indent).append("{\n");
        for (SMAction action : m_globalPreActions.getActions())
        {
            diagram.append(indent).append(indentSize).append(ACTION).append(": ").append(action.getAction()).append("\n");
        }
        diagram.append(indent).append("}\n\n");

        // Write the global pre-match-actions.
        if (m_globalPreMatchActionList.size() > 0)
        {
            diagram.append(indent).append(GLOBALPREMATCHACTIONS).append(":\n");
            diagram.append(indent).append("{\n");
            for (SMTrans tr : m_globalPreMatchActionList)
            {
                diagram.append(StateDiagram.indentLines(tr.toString(), indent + indent));
            }
            diagram.append(indent).append("}\n\n");
        }

        // Write the global post-actions.
        diagram.append(indent).append(GLOBALPOSTACTIONS).append(":\n");
        diagram.append(indent).append("{\n");
        for (SMAction action : m_globalPostActions.getActions())
        {
            diagram.append(indent).append(indentSize).append(ACTION).append(": ").append(action.getAction()).append("\n");
        }
        diagram.append(indent).append("}\n\n");

        // Write the global post-match-actions.
        if (m_globalPostMatchActionList.size() > 0)
        {
            diagram.append(indent).append(GLOBALPOSTMATCHACTIONS).append(":\n");
            diagram.append(indent).append("{\n");
            for (SMTrans tr : m_globalPostMatchActionList)
            {
                diagram.append(StateDiagram.indentLines(tr.toString(), indent + indent));
            }
            diagram.append(indent).append("}\n\n");
        }

        // Write the states.
        for (SMState state : m_stateList)
        {
            diagram.append(indentLines(state.toString(), indent));
            diagram.append("\n");
        }

        // Back out the indent.
        indent = indent.substring(indentSize.length());
        diagram.append("}\n");
        return diagram.toString();
    }

    public void toXml(String fileName) throws XMLStreamException,
            FactoryConfigurationError, IOException
    {
        ArrayList<SMTrans> transList = null;
        SMTrans trans = null;

        // <?xml version="1.0" encoding="UTF-8" ?>
        // That is the default.

        XMLOutputFactory factory = XMLOutputFactory.newInstance();

        XMLStreamWriter writer = factory.createXMLStreamWriter(new FileWriter(fileName));

        // Set up the XML params.
        writer.writeStartDocument("UTF-8", "1.0");
        writer.writeCharacters("\n");
        writer.writeStartElement(STATEDIAGRAM);

        // Write the diagram name.
        // Need to put in a schema here.
        writer.writeAttribute(NAME, m_name);
        writer.writeCharacters("\n");
        
        // Write the start state name.
        if (m_startState != null)
        {
            writer.writeEmptyElement(STARTSTATE);
            writer.writeAttribute(NAME, m_startState.getName());
            writer.writeCharacters("\n");
        }
        
        // Walk the diagram and collect all the matches and actions.
// What was this for?
        


        // Write the global override transitions.
        transList = m_globalOverrideState.getTransitions();
        if (transList.size() > 0)
        {
            writer.writeCharacters("\n");
            writer.writeStartElement(GLOBALOVERRIDETRANS);
            writer.writeCharacters("\n");
            for (SMTrans tr : transList)
            {
                tr.toXml(writer);
            }
            writer.writeEndElement();
            writer.writeCharacters("\n\n");
        }

        // Write the global transitions.
        transList = m_globalState.getTransitions();
        if (transList.size() > 0)
        {
            writer.writeStartElement(GLOBALTRANS);
            writer.writeCharacters("\n");
            for (SMTrans tr : transList)
            {
                tr.toXml(writer);
            }
            writer.writeEndElement();
            writer.writeCharacters("\n\n");
        }

        // Write the global default transition.
        trans = m_globalDefaultState.getDefaultTrans();
        if (trans != null)
        {
            writer.writeStartElement(GLOBALDEFAULTTRANS);
            writer.writeCharacters("\n");
            trans.toXml(writer);
            writer.writeEndElement();
            writer.writeCharacters("\n\n");
        }

        // Write the global pre-actions.
        writer.writeStartElement(GLOBALPREACTIONS);
        writer.writeCharacters("\n");
        for (SMAction action : m_globalPreActions.getActions())
        {
            action.toXml(writer);
            writer.writeCharacters("\n");
        }
        writer.writeEndElement();
        writer.writeCharacters("\n\n");

        // Write the global pre-match-actions.
        if (m_globalPreMatchActionList.size() > 0)
        {
            writer.writeStartElement(GLOBALPREMATCHACTIONS);
            writer.writeCharacters("\n");
            for (SMTrans tr : m_globalPreMatchActionList)
            {
                tr.toXml(writer);
            }
            writer.writeEndElement();
            writer.writeCharacters("\n\n");
        }

        // Write the global post-actions.
        writer.writeStartElement(GLOBALPOSTACTIONS);
        writer.writeCharacters("\n");
        for (SMAction action : m_globalPostActions.getActions())
        {
            action.toXml(writer);
            writer.writeCharacters("\n");
        }
        writer.writeEndElement();
        writer.writeCharacters("\n\n");

        // Write the global post-match-actions.
        if (m_globalPostMatchActionList.size() > 0)
        {
            writer.writeStartElement(GLOBALPOSTMATCHACTIONS);
            writer.writeCharacters("\n");
            for (SMTrans tr : m_globalPostMatchActionList)
            {
                tr.toXml(writer);
            }
            writer.writeEndElement();
            writer.writeCharacters("\n\n");
        }

        // Write the states.
        for (SMState state : m_stateList)
        {
            state.toXml(writer);
        }

        writer.writeEndDocument();
        writer.writeCharacters("\n");
        writer.close();
    }

    /** Index table for states as they are declared. */
    // Not used yet, need to work on fromXml().
    private Hashtable<String, SMState> m_stateIndex;
    private SMState m_currXmlState = null;
    private SMTrans m_currXmlTrans = null;
    private final ArrayList<SMAction> m_currXmlActionList = new ArrayList<SMAction>();
    private final ArrayList<SMTrans> m_currXmlTransList = new ArrayList<SMTrans>();
    private final ArrayList<SMTrans> m_allXmlTransList = new ArrayList<SMTrans>();
    private String m_currXmlStartStateName = null;

    public void fromXml(String fileName) throws ParserConfigurationException,
    SAXException, IOException
    {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        SAXParser saxParser = spf.newSAXParser();
        XMLReader xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler(this);
        xmlReader.parse(convertToFileURL(fileName));
    }

    public void fromXml(InputStream xmlStream) throws ParserConfigurationException,
    SAXException, IOException
    {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        SAXParser saxParser = spf.newSAXParser();
        XMLReader xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler(this);
        xmlReader.parse(new InputSource(xmlStream));
    }

    private static String convertToFileURL(String filename)
    {
        String path = new File(filename).getAbsolutePath();
        if (File.separatorChar != '/')
        {
            path = path.replace(File.separatorChar, '/');
        }

        if (!path.startsWith("/"))
        {
            path = "/" + path;
        }
        return "file:" + path;
    }

    public void startDocument() throws SAXException
    {
        m_stateIndex = new Hashtable<String, SMState>();
    }

    public void startElement(String namespaceURI, String localName,
            String qName, Attributes atts) throws SAXException
    {
        String stName = new String();
        String stMatch = new String();
        String stType = new String();

        switch (localName)
        {
            case STATEDIAGRAM:
                m_name = atts.getValue(NAME);
                break;
            case STARTSTATE:
                m_currXmlStartStateName = new String(atts.getValue(NAME));
                break;
            case STATE:
                stName = atts.getValue(NAME);
                if (m_stateIndex.containsKey(stName))
                {
                    // throw duplicate state name exception.
                    // or, add transitions to the previous state.
                    // For now, just print error message.
                    System.out.println("*** State <" + stName + "> already exists. ***");
                    return;
                }
                m_currXmlState = new SMState(stName);
                m_stateIndex.put(m_currXmlState.getName(), m_currXmlState);
                break;
            case KEY:
                break;
            case ENTRYACTIONS:
            case EXITACTIONS:
            case GLOBALPOSTACTIONS:
            case GLOBALPREACTIONS:
            case POSTACTIONS:
            case PREACTIONS:
            case GLOBALPOSTACTION:
                m_currXmlActionList.clear();
                break;
            case GLOBALPREMATCHACTIONS:
            case GLOBALDEFAULTTRANS:
            case GLOBALTRANS:
            case GLOBALOVERRIDETRANS:
            case GLOBALPOSTMATCHACTIONS:
            case DEFAULT:
                m_currXmlTransList.clear();
                m_currXmlTrans = null;
                break;
            case TRANSITIONS:
            case OVERRIDE:
                m_currXmlTransList.clear();
                break;
            case TRANSITION:
                stMatch = atts.getValue(MATCH);
                stType = atts.getValue(TYPE).toUpperCase();
                stName = atts.getValue(STATE);
                TransType tt = TransType.valueOf(stType);
                // The go to state made not have been read yet, so just
                // init with the name and resolve later.
                SMMatch theMatch = null;
                if (stMatch != null)
                {
                    theMatch = new SMMatch(stMatch);
                }
                m_currXmlTrans = new SMTrans(theMatch, stName, tt);
                m_allXmlTransList.add(m_currXmlTrans);
                break;
            case ACTION:
                // Get match, type, and state attributes.
                // Build transition and add to m_currXmlTransList.
                stName = atts.getValue(NAME);
                SMAction act = new SMAction(stName);
                m_currXmlActionList.add(act);
                break;
                default:
                break;
        }
    }
    
    public void endElement(String uri, String name, String qName)
    {
        switch (name)
        {
            case STATEDIAGRAM:
                // Now that the diagram is finished, go back and put the
                // destination states into all the transitions. As we were
                // reading them, we just stored the destinations as names
                // in the transitions. We should have all the states read
                // in now, so swap out the state names for states.
                for (SMTrans trans : m_allXmlTransList)
                {
                    // Create a SMNextState object for the transitions that
                    // go to another state.
                    if (trans.needsStateResolved())
                    {
                        String stName = trans.getNextStateStr();
                        SMState state = m_stateIndex.get(stName);
                        TransType tt = trans.getTransType();
                        trans.setNextState(new SMNextState(state, tt));
                    }

                    // Now handle the transitions that don't go to another state,
                    // still need to create a SMNextState object.
                    if (trans.getNextState() == null)
                    {
                        TransType tt = trans.getTransType();
                        trans.setNextState(new SMNextState(tt));
                    }
                }

                // Set the start state if we got one.
                if (m_currXmlStartStateName != null)
                {
                    SMState st = m_stateIndex.get(m_currXmlStartStateName);
                    setStartState(st);
                }
                break;
            case STATE:
                addState(m_currXmlState);
                m_currXmlState = null;
                break;
            case ENTRYACTIONS:
                for (SMAction act : m_currXmlActionList)
                {
                    m_currXmlState.addEntryAction(act);
                }
                m_currXmlActionList.clear();
                break;
            case EXITACTIONS:
                for (SMAction act : m_currXmlActionList)
                {
                    m_currXmlState.addExitAction(act);
                }
                m_currXmlActionList.clear();
                break;
            case GLOBALPREACTIONS:
                for (SMAction act : m_currXmlActionList)
                {
                    addGlobalPreAction(act);
                }
                m_currXmlActionList.clear();
                break;
            case GLOBALPREMATCHACTIONS:
                for (SMTrans trans : m_currXmlTransList)
                {
                    addGlobalPreMatchAction(trans);
                }
                m_currXmlTransList.clear();
                break;
            case GLOBALPOSTACTIONS:
                for (SMAction act : m_currXmlActionList)
                {
                    addGlobalPostAction(act);
                }
                m_currXmlActionList.clear();
                break;
            case GLOBALPOSTMATCHACTIONS:
                for (SMTrans trans : m_currXmlTransList)
                {
                    addGlobalPostMatchAction(trans);
                }
                m_currXmlTransList.clear();
                break;
            case GLOBALOVERRIDETRANS:
                for (SMTrans trans : m_currXmlTransList)
                {
                    addGlobalOverrideTrans(trans);
                }
                m_currXmlTransList.clear();
                break;
            case GLOBALTRANS:
                for (SMTrans trans : m_currXmlTransList)
                {
                    addGlobalTrans(trans);
                }
                m_currXmlTransList.clear();
                break;
            case GLOBALDEFAULTTRANS:
                setGlobalDefaultTrans(m_currXmlTransList.get(0));
                m_currXmlTransList.clear();
                break;
            case TRANSITIONS:
                for (SMTrans trans : m_currXmlTransList)
                {
                    if ((m_currXmlState != null) && (trans != null))
                    {
                        m_currXmlState.addTrans(trans);
                    }
                }
                m_currXmlTransList.clear();
                break;
            case OVERRIDE:
                for (SMTrans trans : m_currXmlTransList)
                {
                    if ((m_currXmlState != null) && (trans != null))
                    {
                        m_currXmlState.addOverrideTrans(trans);
                    }
                }
                m_currXmlTransList.clear();
                break;
            case TRANSITION:
                for (SMAction act : m_currXmlActionList)
                {
                    if ((act != null) && (m_currXmlTrans != null))
                    {
                        m_currXmlTrans.addAction(act);
                    }
                }
                m_currXmlActionList.clear();
                m_currXmlTransList.add(m_currXmlTrans);
                m_currXmlTrans = null;
                break;
            case ACTION:
                // Ignore, actions contain only attributes.
                break;
            case DEFAULT:
                m_currXmlState.setDefaultTrans(m_currXmlTransList.get(0));
                m_currXmlTransList.clear();
                break;
            case STARTSTATE:
                // Ignore, start state contains only attributes.
                break;
            default:
                break;
        }
    }

    public void endDocument() throws SAXException
    {
    }

    public void characters(char[] chars, int start, int length)
    {
        StringBuilder str = new StringBuilder(new String());
        for (int i = start; i < (start + length); ++i)
        {
            if (chars[i] != '\n')
            {
                str.append(chars[i]);
            }
        }
        str = new StringBuilder(str.toString().trim());
        if (str.length() > 0)
        {
            // Ignore characters for now.
        }
    }

    public SMState getGlobalOverrideState() { return m_globalOverrideState; }

    public SMState getGlobalState()
    {
        return m_globalState;
    }

    public SMState getGlobalDefaultState()
    {
        return m_globalDefaultState;
    }

    public ArrayList<SMAction> getGlobalPreActions()
    {
        return m_globalPreActions.getActions();
    }

    public ArrayList<SMAction> getGlobalPostActions()
    {
        return m_globalPostActions.getActions();
    }

    public ArrayList<SMTrans> getGlobalPreMatchList()
    {
        return m_globalPreMatchActionList;
    }

    public ArrayList<SMTrans> getGlobalPostMatchList()
    {
        return m_globalPostMatchActionList;
    }

    public ArrayList<SMAction> getUniqueActions()
    {
        ArrayList<SMAction> actions = new ArrayList<SMAction>();
        Hashtable<String, SMAction> uniqueActions = new Hashtable<String, SMAction>();

        for (SMState state : m_stateList)
        {
            getStateActions(uniqueActions, state);
        }

        getStateActions(uniqueActions, m_globalDefaultState);
        getStateActions(uniqueActions, m_globalOverrideState);
        getStateActions(uniqueActions, m_globalState);

        getTransActions(uniqueActions, m_globalPreActions);
        getTransActions(uniqueActions, m_globalPostActions);
        for (SMTrans trans : m_globalPreMatchActionList)
        {
            getTransActions(uniqueActions, trans);
        }
        for (SMTrans trans : m_globalPostMatchActionList)
        {
            getTransActions(uniqueActions, trans);
        }

        // Return the list of unique actions.
        actions.addAll(uniqueActions.values());
        return actions;
    }

    private void getTransActions(Hashtable<String, SMAction> uniqueActions,
            SMTrans trans)
    {
        if (trans == null)
        {
            return;
        }
        for (SMAction action : trans.getActions())
        {
            uniqueActions.put(action.getAction(), action);
        }
    }

    private void getTransMatches(Hashtable<String, SMMatch> uniqueMatches,
            SMTrans trans)
    {
        if (trans == null)
        {
            return;
        }
        SMMatch match = trans.getMatch();
        if (match != null)
        {
            uniqueMatches.put(match.getKey(), match);
        }
    }

    private void getStateActions(Hashtable<String, SMAction> uniqueActions,
            SMState state)
    {
        if (state == null)
        {
            return;
        }
        ArrayList<SMAction> stActions = state.getAllActions();
        for (SMAction action : stActions)
        {
            uniqueActions.put(action.getAction(), action);
        }
    }

    private void getStateMatches(Hashtable<String, SMMatch> uniqueMatches,
            SMState state)
    {
        if (state == null)
        {
            return;
        }
        ArrayList<SMMatch> stMatches = state.getAllMatches();
        for (SMMatch match : stMatches)
        {
            uniqueMatches.put(match.getKey(), match);
        }
    }

    public ArrayList<SMMatch> getUniqueMatches()
    {
        ArrayList<SMMatch> matches = new ArrayList<SMMatch>();
        Hashtable<String, SMMatch> uniqueMatches = new Hashtable<String, SMMatch>();

        // Collect the matches from all the states.
        for (SMState state : m_stateList)
        {
            getStateMatches(uniqueMatches, state);
        }

        // Collect the matches from the global transitions.
        getStateMatches(uniqueMatches, m_globalDefaultState);
        getStateMatches(uniqueMatches, m_globalOverrideState);
        getStateMatches(uniqueMatches, m_globalState);

        // Collect the matches from the global pre and post actions.
        getTransMatches(uniqueMatches, m_globalPreActions);
        getTransMatches(uniqueMatches, m_globalPostActions);
        for (SMTrans trans : m_globalPreMatchActionList)
        {
            getTransMatches(uniqueMatches, trans);
        }
        for (SMTrans trans : m_globalPostMatchActionList)
        {
            getTransMatches(uniqueMatches, trans);
        }

        // Return the list of unique actions.
        matches.addAll(uniqueMatches.values());

        return matches;
    }
}
