package org.giancola.statemachine;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.giancola.statemachine.SMNextState.TransType;
import org.junit.Test;
import org.xml.sax.SAXException;

import static org.junit.Assert.*;

public class StateMachineTest
{
    ArrayList<String> actionsCalled = new ArrayList<String>();


    @Test
    public void testMatchENUM()
    {
        assertNotNull(SMMatch.Oper.AND);
        assertNotNull(SMMatch.Oper.valueOf("AND"));
        assertEquals("AND", SMMatch.Oper.AND.name());

        assertNotNull(SMMatch.Oper.OR);
        assertNotNull(SMMatch.Oper.valueOf("OR"));
        assertEquals("OR", SMMatch.Oper.OR.name());

        assertNotNull(SMMatch.Oper.NOT);
        assertNotNull(SMMatch.Oper.valueOf("NOT"));
        assertEquals("NOT", SMMatch.Oper.NOT.name());

        assertNotNull(SMMatch.Oper.BEGIN);
        assertNotNull(SMMatch.Oper.valueOf("BEGIN"));
        assertEquals("BEGIN", SMMatch.Oper.BEGIN.name());

        assertNotNull(SMMatch.Oper.END);
        assertNotNull(SMMatch.Oper.valueOf("END"));
        assertEquals("END", SMMatch.Oper.END.name());
    }

    @Test
    public void testActionENUM()
    {
        assertNotNull(SMAction.ActionReturn.NORMAL);
        assertNotNull(SMAction.ActionReturn.valueOf("NORMAL"));
        assertEquals("NORMAL", SMAction.ActionReturn.NORMAL.name());

        assertNotNull(SMAction.ActionReturn.TERMINATE);
        assertNotNull(SMAction.ActionReturn.valueOf("TERMINATE"));
        assertEquals("TERMINATE", SMAction.ActionReturn.TERMINATE.name());

        assertNotNull(SMAction.ActionReturn.VETO);
        assertNotNull(SMAction.ActionReturn.valueOf("VETO"));
        assertEquals("VETO", SMAction.ActionReturn.VETO.name());

        assertNotNull(SMAction.ActionReturn.STOPACTIONS);
        assertNotNull(SMAction.ActionReturn.valueOf("STOPACTIONS"));
        assertEquals("STOPACTIONS", SMAction.ActionReturn.STOPACTIONS.name());

        assertNotNull(SMAction.ActionReturn.STOPACTIONSALL);
        assertNotNull(SMAction.ActionReturn.valueOf("STOPACTIONSALL"));
        assertEquals("STOPACTIONSALL", SMAction.ActionReturn.STOPACTIONSALL.name());
    }

    @SuppressWarnings("unused")
    @Test
    public void testStateMachine()
    {
        try
        {
            StateMachine sm = new StateMachine();
            sm = null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    @Test
    public void testStateMachineStateDiagram()
    {
        StateDiagram sd = new StateDiagram();
        StateMachine sm = new StateMachine(sd);
        sm = null;
        sd = null;
    }

    @Test
    public void testStateMachineAddStates()
    {
        StateDiagram sd = new StateDiagram();

        SMState st = new SMState();
        sd.addState(st);
    }

    @Test
    public void testStateMachineAddTransToState()
    {
        SMState st = new SMState();
        SMTrans tr = new SMTrans();

        st.addTrans(tr);
    }

    @Test
    public void testStateDiagramFullDiag()
    {
        StateDiagram sd = genDiagram1();
        assertEquals("Start State", "State1", sd.getStartState().getName());
    }

    @Test
    public void testStateDiagramOps()
    {
        // Create the state diagram and give it to the state machine.
        StateDiagram sd = genDiagram2();
        StateMachine sm = new StateMachine(sd);
        runStateMachine(sm);
    }

    @Test
    public void testSerialize() throws FileNotFoundException, IOException,
            ClassNotFoundException
    {
        StateDiagram sd = genDiagram2();

        FileOutputStream fout = new FileOutputStream("diag.ser");
        ObjectOutputStream out = new ObjectOutputStream(fout);
        out.writeObject(sd);
        out.close();
        fout.close();

        sd = null;

        FileInputStream fin = new FileInputStream("diag.ser");
        ObjectInputStream in = new ObjectInputStream(fin);
        sd = (StateDiagram) in.readObject();
        in.close();
        fin.close();

        // Run the state diagram.
        StateMachine sm = new StateMachine(sd);
        runStateMachine(sm);
    }

    @Test
    public void testNextExpected()
    {
        StateDiagram sd = genDiagram2();
        StateMachine sm = new StateMachine(sd);
        runStateMachine(sm);
        ArrayList<SMMatch> nextMatches = sm.getNextExpected();
        assertEquals("Expected Matches", 6, nextMatches.size());
    }

    @Test
    public void testToString()
    {
        StateDiagram sd = genDiagram2();
        String sdString = sd.toString();
        String testString = new String("Default:\n            <any>, loopback");
        int pos = sdString.indexOf(testString);
        assertEquals("Expected substring", 948, pos);
    }

    @Test
    public void testXMLReaderFile() throws XMLStreamException,
            javax.xml.parsers.FactoryConfigurationError, IOException,
            ParserConfigurationException, SAXException
    {
        StateDiagram sd = genDiagram2();
        sd.toXml("sdOutTestFileReader.xml");
        StateDiagram sd1 = new StateDiagram();
        sd1.fromXml("sdOutTestFileReader.xml");
        sd1.toXml("sdOutTestFileReaderAfter.xml");
    }

    @Test
    public void testXMLReaderStream() throws XMLStreamException,
            javax.xml.parsers.FactoryConfigurationError, IOException,
            ParserConfigurationException, SAXException
    {
        StateDiagram sd = genDiagram2();
        sd.toXml("sdOutTestStreamReader.xml");
        StateDiagram sd1 = new StateDiagram();

        try (InputStream in = new BufferedInputStream(new FileInputStream("sdOutTestStreamReader.xml")))
        {
            sd1.fromXml(in);
            sd1.toXml("sdOutTestStreamReaderAfter.xml");
        }
    }

    @Test
    public void testToXML() throws XMLStreamException,
            javax.xml.parsers.FactoryConfigurationError, IOException
    {
        StateDiagram sd = genDiagram2();
        sd.toXml("sdOutTestToXml.xml");
    }

    @Test
    public void testUniqueMatches()
    {
        StateDiagram sd = genDiagram2();
        ArrayList<SMMatch> uniqueMatches = sd.getUniqueMatches();
        assertEquals("Unique Matches", 8, uniqueMatches.size());
    }

    @Test
    public void testUniqueActions()
    {
        StateDiagram sd = genDiagram2();
        ArrayList<SMAction> uniqueActions = sd.getUniqueActions();
        assertEquals("UniqueActions", 12, uniqueActions.size());
    }

    @Test
    public void testCallActions() throws NoSuchMethodException,
            SecurityException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException
    {
        ArrayList<SMAction> actions = new ArrayList<SMAction>();
        StateDiagram sd = genDiagram2();
        StateMachine sm = new StateMachine(sd);
        SMMatch ma1 = new SMMatch("MouseMove");

        // Check the startup conditions.
        assertEquals("Start State", "State1", sm.getStartState().getName());
        assertEquals("Curr State", "State1", sm.getCurrState().getName());

        // Send an input and check the state and actions.
        actions = sm.inputReturnActions(ma1);
        assertEquals("After Trans", "State2", sm.getCurrState().getName());
        assertEquals("Actions", 7, actions.size());
        assertEquals("The Action", "preStartRubberBand", actions.get(0).getAction());
        assertEquals("The Action", "biteMyShinyMetalAss", actions.get(3).getAction());
        assertEquals("The Action", "walkThisWay", actions.get(4).getAction());

        // Call the actions in the list.
        sm.callActionList(this, actions, ma1);
    }

    @Test
    public void testActionCallbacks() throws NoSuchMethodException,
            SecurityException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException
    {
        StateDiagram sd = genDiagram2();
        StateMachine sm = new StateMachine(sd);
        actionsCalled.clear();
        int index = 0;

        // Transition with actions.
        assertEquals("Before Trans, with acitons", "State1", sm.getCurrState().getName());
        SMMatch ma1 = new SMMatch("MouseMove");
        sm.input(this, ma1);
        assertEquals("After Trans", "State2", sm.getCurrState().getName());
        assertEquals(7, actionsCalled.size());
        assertEquals("preStartRubberBand", actionsCalled.get(index++));
        assertEquals("preDragRubberBand", actionsCalled.get(index++));
        assertEquals("startRubberBand", actionsCalled.get(index++));
        assertEquals("biteMyShinyMetalAss", actionsCalled.get(index++));
        assertEquals("walkThisWay", actionsCalled.get(index++));
        assertEquals("postStartRubberBand", actionsCalled.get(index++));
        assertEquals("postDragRubberBand", actionsCalled.get(index++));
        actionsCalled.clear();

        // Transition with no actions.
        assertEquals("Before 2nd Trans, no actions", "State2", sm.getCurrState().getName());
        SMMatch ma2 = new SMMatch("SwingLog");
        sm.input(this, ma2);
        assertEquals("After Trans", "State1", sm.getCurrState().getName());
        assertEquals(6, actionsCalled.size());
        index = 0;
        assertEquals("preStartRubberBand", actionsCalled.get(index++));
        assertEquals("preDragRubberBand", actionsCalled.get(index++));
        assertEquals("cryHavocAndLetSlipTheDogsOfWar", actionsCalled.get(index++));
        assertEquals("startRubberBand", actionsCalled.get(index++));
        assertEquals("postStartRubberBand", actionsCalled.get(index++));
        assertEquals("postDragRubberBand", actionsCalled.get(index++));
        actionsCalled.clear();
    }

    @Test
    public void testPrePostActions()
    {
        StateDiagram sd = genDiagram3();
        StateMachine sm = new StateMachine(sd);
        SMMatch ma1 = new SMMatch("Move1");
        SMMatch ma2 = new SMMatch("Move2");
        SMMatch ma3 = new SMMatch("Move3");
        SMMatch ma4 = new SMMatch("Move4");
       // Here
        sm.inputReturnActions(ma1);

        sm.inputReturnActions(ma1);

        sm.inputReturnActions(ma3);

        sm.inputReturnActions(ma3);

        sm.inputReturnActions(ma2);

        sm.inputReturnActions(ma4);
    }

    @Test
    public void testRegexMatches()
    {
        StateDiagram sd = genDiagram4();
        StateMachine sm = new StateMachine(sd);
        SMMatch ma1 = new SMMatch("Move1");
        SMMatch ma2 = new SMMatch("Move2");
        SMMatch ma3 = new SMMatch("Move9");
        SMMatch ma4 = new SMMatch("BlingIsAll");
        SMMatch ma5 = new SMMatch("Bling");
        SMMatch ma6 = new SMMatch("Blong");
        SMMatch maR = new SMMatch("Return");

        sm.inputReturnActions(ma1);
        assertEquals("After Move1 Trans", "State2", sm.getCurrState().getName());
        sm.inputReturnActions(maR);
        assertEquals("After Move1 Return", "State1", sm.getCurrState().getName());

        sm.inputReturnActions(ma2);
        assertEquals("After Move2 Trans", "State2", sm.getCurrState().getName());
        sm.inputReturnActions(maR);
        assertEquals("After Move2 Return", "State1", sm.getCurrState().getName());

        sm.inputReturnActions(ma3);
        assertEquals("After Move9 Trans", "State2", sm.getCurrState().getName());
        sm.inputReturnActions(maR);
        assertEquals("After Move9 Return", "State1", sm.getCurrState().getName());

        sm.inputReturnActions(ma4);
        assertEquals("After BlingIsAll Trans", "State2", sm.getCurrState().getName());
        sm.inputReturnActions(maR);
        assertEquals("After BlingIsAll Return", "State1", sm.getCurrState().getName());

        sm.inputReturnActions(ma5);
        assertEquals("After Bling Trans", "State2", sm.getCurrState().getName());
        sm.inputReturnActions(maR);
        assertEquals("After Bling Return", "State1", sm.getCurrState().getName());

        sm.inputReturnActions(ma6);
        assertEquals("After Blong Trans", "State4", sm.getCurrState().getName());
        sm.inputReturnActions(ma6);
        assertEquals("After Blong Return", "State2", sm.getCurrState().getName());
    }

    /* Action functions. */
    /*---------------------------------------------------------------------*/
    // The parameter to the action functions should be an object that
    // includes relevant info from the caller's context, like mouse
    // position for a UI state machine. The "Object... params" is
    // used because that seems to make reflection work. And I don't
    // know why.
    public void startRubberBand(String query, Object... params)
    {
        actionsCalled.add("startRubberBand");
    }

    public void dragRubberBand(String query, Object... params)
    {
        actionsCalled.add("dragRubberBand");
    }

    public void preStartRubberBand(String query, Object... params)
    {
        actionsCalled.add("preStartRubberBand");
    }

    public void preDragRubberBand(String query, Object... params)
    {
        actionsCalled.add("preDragRubberBand");
    }

    public void postStartRubberBand(String query, Object... params)
    {
        actionsCalled.add("postStartRubberBand");
    }

    public void postDragRubberBand(String query, Object... params)
    {
        actionsCalled.add("postDragRubberBand");
    }

    public void endRubberBand(String query, Object... params)
    {
        actionsCalled.add("endRubberBand");
    }

    public void goAheadMakeMyDay(String query, Object... params)
    {
        actionsCalled.add("goAheadMakeMyDay");
    }

    public void walkThisWay(String query, Object... params)
    {
        actionsCalled.add("walkThisWay");
    }

    public void cryHavocAndLetSlipTheDogsOfWar(String query, Object... params) { actionsCalled.add("cryHavocAndLetSlipTheDogsOfWar"); }

    public void doSomeOfThatPilotShit(String query, Object... params)
    {
        actionsCalled.add("doSomeOfThatPilotShit");
    }

    public void biteMyShinyMetalAss(String query, Object... params)
    {
        actionsCalled.add("biteMyShinyMetalAss");
    }

    /* Helper functions. */
    /*---------------------------------------------------------------------*/

    private void runStateMachine(StateMachine sm)
    {
        ArrayList<SMAction> actions = new ArrayList<SMAction>();
        SMMatch ma1 = new SMMatch("MouseMove");

        // Check the startup conditions.
        assertEquals("Start State", "State1", sm.getStartState().getName());
        assertEquals("Curr State", "State1", sm.getCurrState().getName());

        // Send an input and check the state and actions.
        actions = sm.inputReturnActions(ma1);
        assertEquals("After Trans", "State2", sm.getCurrState().getName());
        assertEquals("Action Count", 7, actions.size());
        assertEquals("The Action", "preStartRubberBand", actions.get(0).getAction());
        assertEquals("The Action", "biteMyShinyMetalAss", actions.get(3).getAction());
        assertEquals("The Action", "walkThisWay", actions.get(4).getAction());
    }

    private StateDiagram genDiagram1()
    {
        StateDiagram sd = new StateDiagram("Diagram 1");
        SMState st1 = new SMState("State1");
        SMState st2 = new SMState("State2");
        SMMatch ma1 = new SMMatch("SomeMessage");
        SMTrans tr1 = new SMTrans(ma1, st2);
        SMAction ac1 = new SMAction("goRavens");
        tr1.addAction(ac1);
        st1.addTrans(tr1);
        sd.addState(st1);
        sd.addState(st2);
        sd.setStartState(st1);
        return sd;
    }

    private StateDiagram genDiagram2()
    {
        StateDiagram sd = new StateDiagram("Diagram 2");
        ArrayList<SMAction> actions = new ArrayList<SMAction>();
        SMTrans dt = null;

        // Create states.
        SMState st1 = new SMState("State1");
        SMState st2 = new SMState("State2");
        SMState st3 = new SMState("State3");
        SMState st4 = new SMState("State4");

        // Create matches.
        SMMatch ma1 = new SMMatch("MouseMove");
        SMMatch ma2 = new SMMatch("LeftButtonDown");
        SMMatch ma3 = new SMMatch("LeftButtonUp");
        SMMatch ma4 = new SMMatch("RightButtonDown");
        SMMatch ma5 = new SMMatch("RightButtonUp");
        SMMatch ma6 = new SMMatch("NuttinHoney");
        SMMatch ma7 = new SMMatch("IShotTheSherrif");
        SMMatch ma8 = new SMMatch("COWEIEIO");
        SMMatch ma9 = new SMMatch("SwingLow");

        // Create actions.
        SMAction ac1 = new SMAction("startRubberBand");
        SMAction ac2 = new SMAction("dragRubberBand");
        SMAction ac3 = new SMAction("endRubberBand");
        SMAction ac4 = new SMAction("goAheadMakeMyDay");
        SMAction ac5 = new SMAction("walkThisWay");
        SMAction ac6 = new SMAction("cryHavocAndLetSlipTheDogsOfWar");
        SMAction ac7 = new SMAction("doSomeOfThatPilotShit");
        SMAction ac8 = new SMAction("biteMyShinyMetalAss");
        SMAction ac9 = new SMAction("preStartRubberBand");
        SMAction ac10 = new SMAction("preDragRubberBand");
        SMAction ac11 = new SMAction("postStartRubberBand");
        SMAction ac12 = new SMAction("postDragRubberBand");

        // Build state 1.
        actions.add(ac1);
        st1.buildTrans(ma1, st2, actions);
        actions.clear();
        actions.add(ac2);
        st1.buildTrans(ma2, st3, actions);
        actions.clear();

        // Loopback on ma3.
        dt = new SMTrans(ma3);
        dt.addAction(ac5);
        dt.addAction(ac8);
        st1.addTrans(dt);

        // Default is a loopback transition.
        st1.setDefaultTrans(new SMTrans());

        // Build state 2.
        actions.clear();
        st2.buildTrans(ma9, st1, actions);
        actions.clear();
        actions.add(ac3);
        st2.buildTrans(ma1, st4, actions);
        actions.clear();
        dt = new SMTrans(ma2);
        dt.addAction(ac6);
        st2.addTrans(dt);
        actions.clear();
        actions.add(ac7);
        actions.add(ac1);
        actions.add(ac5);
        st2.buildTrans(ma3, st3, actions);
        actions.clear();
        actions.add(ac2);
        st2.buildTrans(ma4, st1, actions);
        actions.clear();
        actions.add(ac8);
        st2.buildTrans(ma5, st2, actions);
        st2.addEntryAction(ac8);
        st2.addEntryAction(ac5);
        st2.addExitAction(ac6);
        st2.addExitAction(ac1);

        // An override transition on ma4 to supersede the regular one.
        SMTrans tr4c3 = new SMTrans(ma4, st3, TransType.CALL);
        tr4c3.addAction(ac6);
        st2.addOverrideTrans(tr4c3);
        st2.addOverrideTrans(new SMTrans(ma3, st4));
        st2.setDefaultTrans(new SMTrans(st1));

        // Build state 3.
        actions.clear();
        actions.add(ac8);
        st3.buildTrans(ma1, st1, actions);
        actions.clear();
        actions.add(ac3);
        st3.buildTrans(ma2, st2, actions);
        actions.clear();
        actions.add(ac2);
        st3.buildTrans(ma3, st3, actions);

        // A return transition on ma4.
        SMTrans tr4r = new SMTrans(ma4, TransType.RETURN);
        tr4r.addAction(ac6);
        st3.addTrans(tr4r);

        // Build state 4.
        actions.clear();
        actions.add(ac4);
        st4.buildTrans(ma1, st3, actions);
        actions.clear();
        actions.add(ac6);
        actions.add(ac5);
        actions.add(ac4);
        actions.add(ac8);
        actions.add(ac3);
        st4.buildTrans(ma2, st2, actions);
        actions.clear();
        actions.add(ac5);
        st4.buildTrans(ma3, st4, actions);
        actions.clear();
        actions.add(ac8);
        st4.buildTrans(ma4, st4, actions);

        // Add states to diagram.
        sd.addState(st1);
        sd.addState(st2);
        sd.addState(st3);
        sd.addState(st4);

        // Global override transitions.
        SMTrans trgo1 = new SMTrans(ma6, TransType.LOOPBACK);
        trgo1.addAction(ac8);
        sd.addGlobalOverrideTrans(trgo1);

        // Global transitions.
        SMTrans trg1 = new SMTrans(ma7, TransType.LOOPBACK);
        trg1.addAction(ac4);
        trg1.addAction(ac1);
        sd.addGlobalTrans(trg1);

        // Global default transition.
        SMTrans trgd1 = new SMTrans(ma8, TransType.LOOPBACK);
        trgd1.addAction(ac4);
        trgd1.addAction(ac1);
        sd.setGlobalDefaultTrans(trgd1);

        // Global pre-actions.
        sd.addGlobalPreAction(ac9);
        sd.addGlobalPreAction(ac10);

        // Global post-actions.
        sd.addGlobalPostAction(ac11);
        sd.addGlobalPostAction(ac12);

        // Set the start state.
        sd.setStartState(st1);

        return sd;
    }

    private StateDiagram genDiagram3()
    {
        StateDiagram sd = new StateDiagram("Diagram 3");
        SMTrans dt = null;

        // Create states.
        SMState st1 = new SMState("State1");
        SMState st2 = new SMState("State2");

        // Create matches.
        SMMatch ma1 = new SMMatch("Move1");
        SMMatch ma2 = new SMMatch("Move2");
        SMMatch ma3 = new SMMatch("Move3");
        SMMatch ma4 = new SMMatch("Move4");

        // Create actions.
        SMAction gpre1 = new SMAction("GlobalPre1");
        SMAction gpre2 = new SMAction("GlobalPre2");
        SMAction gpost = new SMAction("GlobalPost");
        SMAction gprem1 = new SMAction("GlobalPreMove1");
        SMAction gprem2 = new SMAction("GlobalPreMove2");
        SMAction gpostm1 = new SMAction("GlobalPostMove1");
        SMAction gpostm3 = new SMAction("GlobalPostMove3");
        SMAction s1pre = new SMAction("State1Pre");
        SMAction s1post = new SMAction("State1Post");
        SMAction s2pre = new SMAction("State2Pre");
        SMAction s2post = new SMAction("State2Post");
        SMAction s1reg = new SMAction("State1Reg");
        SMAction s2reg = new SMAction("State2Reg");
        SMAction gd = new SMAction("GlobalDefault");

        // Add global pre-actions.
        sd.addGlobalPreAction(gpre1);
        sd.addGlobalPreAction(gpre2);
        sd.addGlobalPostAction(gpost);
        sd.addGlobalPreMatchAction(ma1, gprem1);
        sd.addGlobalPreMatchAction(ma2, gprem2);
        sd.addGlobalPostMatchAction(ma1, gpostm1);
        sd.addGlobalPostMatchAction(ma3, gpostm3);

        // Global default transition.
        SMTrans trgd1 = new SMTrans(null, TransType.LOOPBACK);
        trgd1.addAction(gd);
        sd.setGlobalDefaultTrans(trgd1);

        // Build state 1.
        dt = new SMTrans(ma1, st2);
        dt.addAction(s1reg);
        st1.addTrans(dt);
        dt = new SMTrans(ma3);
        dt.addAction(s1reg);
        st1.addTrans(dt);
        st1.addPreAction(s1pre);
        st1.addPostAction(s1post);
        dt = new SMTrans();
        st1.setDefaultTrans(dt);

        // Build state 2.
        dt = new SMTrans(ma1, st1);
        dt.addAction(s2reg);
        st2.addTrans(dt);
        dt = new SMTrans(ma4);
        dt.addAction(s2reg);
        dt.addAction(s1reg);
        st2.addTrans(dt);
        st2.addPreAction(s2pre);
        st2.addPostAction(s2post);
        dt = new SMTrans();
        st2.setDefaultTrans(dt);

        // Add states to diagram.
        sd.addState(st1);
        sd.addState(st2);

        // Set the start state.
        sd.setStartState(st1);

        return sd;
    }

    private StateDiagram genDiagram4()
    {
        StateDiagram sd = new StateDiagram("Diagram 4");
        SMTrans dt = null;

        // Create states.
        SMState st1 = new SMState("State1");
        SMState st2 = new SMState("State2");
        SMState st3 = new SMState("TookDefault");
        SMState st4 = new SMState("State4");

        // Create matches.
        SMMatch ma1 = new SMMatch("Move1");
        SMMatch ma2 = new SMMatch("Move2");
        SMMatch ma3 = new SMMatch("Move.");
        SMMatch ma4 = new SMMatch("Bling.*");
        SMMatch ma5 = new SMMatch("Return");
        SMMatch ma6 = new SMMatch("^Bl((?!i).)*$");

        // Build state 1.
        dt = new SMTrans(ma1, st2);
        st1.addTrans(dt);
        dt = new SMTrans(ma2, st2);
        st1.addTrans(dt);
        dt = new SMTrans(ma3, st2);
        st1.addTrans(dt);
        dt = new SMTrans(ma4, st2);
        st1.addTrans(dt);
        dt = new SMTrans(ma6, st4);
        st1.addTrans(dt);

        // Build state 2.
        dt = new SMTrans(ma5, st1);
        st2.addTrans(dt);

        // Build state 3.
        dt = new SMTrans(ma5, st1);
        st3.addTrans(dt);

        // Build state 4.
        dt = new SMTrans(ma4, st1);
        st4.addTrans(dt);
        dt = new SMTrans(ma6, st2);
        st4.addTrans(dt);

        // Global default transition.
        SMTrans trgd1 = new SMTrans(null, st3);
        sd.setGlobalDefaultTrans(trgd1);

        // Add states to diagram.
        sd.addState(st1);
        sd.addState(st2);
        sd.addState(st3);

        // Set the start state.
        sd.setStartState(st1);
        return sd;
    }
}
