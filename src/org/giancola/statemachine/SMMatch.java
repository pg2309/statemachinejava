package org.giancola.statemachine;

import java.io.Serializable;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class SMMatch implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public enum Oper
    {
        AND, OR, NOT, BEGIN, END
    }

    private final String m_key;
    private final Pattern m_pattern;

    public SMMatch()
    {
        m_key = new String();
        m_pattern = Pattern.compile(m_key);
    }

    public SMMatch(String key)
    {
        m_key = key;
        m_pattern = Pattern.compile(m_key);
    }

    public String getKey()
    {
        return m_key;
    }

    public String toString()
    {
        String match = new String();

        match += StateDiagram.MATCH;
        match += ": <" + m_key + ">";
        return match;
    }

    public Boolean testMatch(String match)
    {
        if (m_key.length() <= 0)
        {
            return false;
        }
        return m_pattern.matcher(match).matches();
    }

    public boolean testMatch(SMMatch input)
    {
        return testMatch(input.m_key);
    }

    public void toXml(XMLStreamWriter writer) throws XMLStreamException
    {
        writer.writeStartElement(StateDiagram.MATCH);
        writer.writeStartElement(StateDiagram.KEY);
        writer.writeCharacters(m_key);
        writer.writeEndElement();
        writer.writeEndElement();
    }

}
