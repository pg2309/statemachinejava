package org.giancola.statemachine;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Stack;

import org.giancola.statemachine.SMNextState.TransType;

// ToDo:
//    * In XML diagram, allow string/numeric parameters to actions. That would
//      be cool!
// Bugs:
//    * Doesn't execute the pre/post actions. Dammit!

public class StateMachine implements Serializable
{
    private static final long serialVersionUID = 1L;
    protected StateDiagram m_sd = null;
    protected SMState m_currState = null;
    protected Stack<SMState> m_callStack = new Stack<SMState>();

    public StateMachine(StateDiagram m_sd)
    {
        super();
        this.m_sd = m_sd;
        m_currState = m_sd.getStartState();
    }

    public StateMachine()
    {
        super();
    }

    public SMState setCurrState(SMState currState)
    {
        SMState oldState = m_currState;
        m_currState = currState;
        return oldState;
    }

    public SMState getCurrState()
    {
        return m_currState;
    }

    public String getName()
    {
        return m_sd.getName();
    }

    /**
     Receive an input message in the state machine.

     This function receives a message to the state machine and changes state,
     if indicated by the diagram. The actions incurred by this input are
     executed directly in the caller during this function's activity. The
     function does not return until all actions have been executed.

     TODO This is where the "reflection", or whatever Java magic, goes.
     */
    public void input(Object callObj, SMMatch input)
        throws NoSuchMethodException, SecurityException,
                          IllegalAccessException, IllegalArgumentException,
                          InvocationTargetException
    {
        ArrayList<SMAction> actions = inputReturnActions(input);
        if ((actions != null) && (actions.size() > 0))
        {
            this.callActionList(callObj, actions, input);
        }
    }

    /**
     Receive an input message in the state machine.
     
     This function receives a message to the state machine and changes state,
     if indicated by the diagram. The actions incurred by this input are
     returned to the caller when this function finishes. The caller is
     responsible for executing the actions on the returned action list.
     */
    public ArrayList<SMAction> inputReturnActions(SMMatch input)
    {
        // First, try the global override transitions.
        SMTrans tr = m_sd.getGlobalOverrideState().execute(input);
        if (tr != null)
        {
            return takeTransition(m_currState/*m_sd.getGlobalOverrideState()*/, tr, input);
        }

        // Next, try the state's transitions.
        tr = m_currState.execute(input);
        if (tr != null)
        {
            return takeTransition(m_currState, tr, input);
        }

        // Then, the global transitions.
        tr = m_sd.getGlobalState().execute(input);
        if (tr != null)
        {
            return takeTransition(m_currState/*m_sd.getGlobalState()*/, tr, input);
        }

        // And finally, the global default transition.
        tr = m_sd.getGlobalDefaultState().execute(input);
        if (tr != null)
        {
            return takeTransition(m_currState/*m_sd.getGlobalDefaultState()*/, tr, input);
        }

        return null;
    }

    private ArrayList<SMAction> takeTransition(SMState currState, SMTrans tr, SMMatch input)
    {
        ArrayList<SMAction> accs = new ArrayList<SMAction>();
        SMNextState ns = tr.getNextState();
        SMState newState = ns.getNext();
        TransType ty = ns.getType();

        switch (ty)
        {
        case GOTO:
            accs.addAll(collectActions(currState, tr, newState, input));
            break;
        case CALL:
            m_callStack.push(currState);
            accs.addAll(collectActions(currState, tr, newState, input));
            break;
        case RETURN:
            newState = m_callStack.pop();
            accs.addAll(collectActions(currState, tr, newState, input));
            break;
        case NONE:
        case LOOPBACK:
            newState = m_currState;
            accs.addAll(collectActions(currState, tr, newState, input));
            break;
        }
        m_currState = newState;
        return accs;
    }

    private ArrayList<SMAction> collectActions(SMState currState, SMTrans tr,
            SMState newState, SMMatch input)
    {
        SMTrans mtr = null;

        // Get the global pre-actions.
        ArrayList<SMAction> accs = new ArrayList<SMAction>(m_sd.getGlobalPreActions());

        // Get the global pre-match-action.
        mtr = getMatchingTrans(input, m_sd.getGlobalPreMatchList());
        if (null != mtr)
        {
            accs.addAll(mtr.getActions());
        }

        // Get the state actions.
        if ((newState == currState) || (newState == null))
        {
            // Staying in the same state, get state's pre-actions.
            accs.addAll(currState.getPreActions());

            // Get transition actions.
            accs.addAll(tr.getActions());

            // Get state's post-actions.
            accs.addAll(currState.getPostActions());
        }
        else
        {
            // Going to a new state, get the current state's pre-actions.
            accs.addAll(currState.getPreActions());

            // Get the current state's exit actions.
            accs.addAll(currState.getExitActions());

            // Get the transition actions.
            accs.addAll(tr.getActions());

            // Get the new state's entry actions.
            accs.addAll(newState.getEntryActions());

            // Get the new state's post-actions.
            accs.addAll(newState.getPostActions());
        }

        // Get the global post-match-actions.
        mtr = getMatchingTrans(input, m_sd.getGlobalPostMatchList());
        if (null != mtr)
        {
            accs.addAll(mtr.getActions());
        }

        // Get the global post-actions.
        accs.addAll(m_sd.getGlobalPostActions());
        return accs;
    }

    public SMTrans getMatchingTrans(SMMatch input, ArrayList<SMTrans> transList)
    {
        for (SMTrans tr : transList)
        {
            if (tr.isMatch(input))
            {
                return tr;
            }
        }
        return null;
    }

    public SMState getStartState()
    {
        return m_sd.getStartState();
    }

    @Override
    public String toString()
    {
        String machine = new String();

        machine += "State Machine\n";
        machine += "Current state: " + m_currState.getName() + "\n";
        machine += "Global Override: " + "\n";
        machine += "Global: " + "\n";
        machine += "Global Default: " + "\n";
        machine += "\n";
        machine += m_sd.toString();
        return machine;
    }

    public ArrayList<SMAction> getUniqueActions()
    {
        return m_sd.getUniqueActions();
    }

    public ArrayList<SMMatch> getUniqueMatches()
    {
        return m_sd.getUniqueMatches();
    }

    public ArrayList<SMMatch> getNextExpected()
    {
        ArrayList<SMMatch> matches = m_currState.getAllMatches();
        Hashtable<String, SMMatch> uniqueMatches = new Hashtable<String, SMMatch>();
        for (SMMatch match : matches)
        {
            uniqueMatches.put(match.getKey(), match);
        }
        matches.clear();
        matches.addAll(uniqueMatches.values());
        return matches;
    }

    // ToDo: Need to handle non-existent action functions.
    public void callActionList(Object callObj, ArrayList<SMAction> actions,
            SMMatch input) throws NoSuchMethodException, SecurityException,
           IllegalAccessException, IllegalArgumentException,
           InvocationTargetException
    {
        Object[] obj = new Object[2];
        obj[0] = "object1";
        obj[1] = "object2";

        @SuppressWarnings("rawtypes")
        Class[] param = new Class[2];
        param[0] = String.class;
        param[1] = Object[].class; // how to define the second parameter as array

        for (SMAction act : actions)
        {
            Method testParamMethod = callObj.getClass().getDeclaredMethod(act.getAction(), param);
            // ToDo: Need to pass the input as args to the action.
            testParamMethod.invoke(callObj, "", obj);
        }
    }

    public static void main(String[] args)
    {
        System.out.println("This is StateMachine.\n");
    }
}

// The following notes about State Machine plans.

/*
General notes on the design of this state machine: This implementation of a
state machine (CSM) qualifies as both Mealy and Moore automata. A Mealy
automata executes actions upon state transitions. They occur "in between"
states. A Moore automata executes actions upon entry into a new state. They
occur "in" a single state, the terminal state of the transition. CSM can
execute actions either way, or both. Transitions have their own lists of
actions that will be executed while changing states. States also have lists
of actions that will be executed upon entry (or exit) to the state. During
execution of actions on the transition the current state is the "previous"
state. This is done to allow transition actions to veto the transition.
Technically, that isn't a proper Mealy automata. So sue me.
 
Transition search order (In each list, if the transition is vetoed, continue
searching for a matching transition. Each matching transition is executed
until one of them isn't vetoed by an action.)
 
Global Override List State List Global List State Default (single transition,
no match condition) Global Default (single transition, no match condition)

//proposed new trans search order // State List // State Default (single
transition, no match condition) // Global List // Global Default (single
transition, no match condition)
 
 
 
Action execution order

Global Pre-Execution Action List 
Global Pre-Execution List (transition with match condition but no state change) 
State Exit Action List (current state, if leaving the state) 
Transition Action List (actions occur in the "previous" state, last chance for veto) 
State Entry Action List (new state, if entering new state) 
Global Post-Execution List (transition with match condition but no state change) 
Global Post-Execution Action List





High o Input queue - Actions can post new input messages that will be
executed at the end of the current input processing.

o Multiple matching transitions - A particular match can occur more than once
in a transition list (state or global). The first matching transition that is
not vetoed is taken.

o Transition veto - An action can veto a transition, leaving the state
machine in the same state. Any side effects caused by the actions executed up
to the veto are not reversed.

High o Subroutine call - Call transitions allow a section of the state
diagram to be executed from multiple points in the diagram. At the end of the
subroutine section, the next state will be the one specified as the return
state in the call transition.

o Random transitions - A chooser for picking from multiple transitions that
have the same match key. The random chooser is able to pick again when a
chosen transition is vetoed. This is sorta like a Markov chain, in reverse
(kinda). The probability factors on each of a set of matching transitions is
the probability of taking that transition over the others when the input
matches all in that set.

Partial o Serialization - State diagrams are now serialized using the
BGSerialize classes. There are no MFC dependencies.

High o Expressions in match conditions - Transitions may now contain lists of
match conditions containing And, Or, Not, Begin (left paren) and End (right
paren) operators. When such a transition is tested, all the matches in the
list will be executed applying the operators to the results as specified.

High o Use SMCompiler to compile state diagrams into binary archives. The
compiler generates a header file containing the symbols defined in the state
diagram. See "SM Diagram Grammer.txt" in the SMCompiler directory for syntax
specification.

o A class containing a state machine may now load a binary state diagram file
at run time. The callback functions may be associated at run time.

o It is no longer necessary to sub-class under the CSM class for the callback
functions to work. Callback functions are connected using the
_CMM_ATTACH_CALLBACK macro in MM.h.

o SMDriver will now display a state diagram from a binary archive file. It
will take a file name on the command line so that it can be the associated
application for the SDI and SDO file types.

o The state machine now prints the names of inputs, matches, and actions in
the trace file.



To Do:

o Make a second level of serialization for applications that don't need to
save and reload state diagrams. This would just save the current state, the
state return stack and any queued inputs.

Done o Separate state diagram storage from current state and individual
action storage. This will allow many instances of a state machine to share a
common diagram and still have their own local state memory.

o Do some kind of error reporting for invalid match expressions.

o Make Input() return a flag indicating whether a state change has occurred.
Also return a pointer to the new state.

o Create a function in the state (possibly other places too) that returns a
list of the matches on the state's transitions. This will be a list of
"what's expected next".

o There could be a context (not CMMContext) pointer passed into the input,
the actions and state machine could store instance local data there. The
state diagram would then be created only with the first instance.

o Allow a state to contain a state machine which determines the transition to
be taken.

Done o Use genetic algorithm to evolve state diagrams.

o Allow a state to contain a neural network which determines the transition
to be taken.

o Allow an action to cleanly force an immediate state change (May not need
this now that the input queue is working)

o Provide an "action list" pointer so that actions being executed in an
action list can pass data to subsequent actions in the list.

Cool o Implement Hierarchical states allowing states to inherit transitions
from other states. When a state doesn't have a transition matching the
current input, it will pass that input to its "parent" state who will try to
match the input. This has the effect of allowing states to have different
sets of "global" transitions. Also allow a "start state" inside each "parent"
state. If a transition goes to a "parent" state, that parent automatically
transitions to its internal start state.

Cool o Allow the machine to exist in a set of states. When there are multiple
matching transitions, take all of them and remember all the resulting states.
Actions will then need a way to say "drop this line of transitions from the
set of current states." May need to queue up the actions (except the "drop"
action) to be executed when the current state collapses to a single state.

o Add a state diagram interpreter and debugger to SMDriver.

o Add a graphical state diagram editor to SMDriver.

o Figure out some way to debug states and transitions when debugging a
program containing a state machine. Maybe just make pointers to the current
transition, previous and current states and other junk in the CSM class.
These could then be seen by the C++ debugger.

o Need local memory for states. Actions should be able to read/set state's
local variables. Need syntax for declaring state's variables.

o Make the "from" and "to" states of a transition available to actions.
Possibly add these pointers to the CMMInput object, or pass a pointer to the
CSMTransition to the actions.

o Make auto-adjusting probabilities on random transitions. In a random
transition group, reduce the probability of the chosen transition and
increase the probability of those not chosen every time one of the group is
executed. This will make an unlikely transition more and more likely every
time it isn't taken. Create a special type of action to do the adjustment.
Then another special type of action could be executed to reset to the
original probability settings.

o Actions should be given a pointer to the transition (or global/state
entry/exit list) executing them. They should know whether they are being
executed by a regular or default state transition, or an override, regular or
default global transition. They should also be told whether the transition is
one of a multiple set. If so, They should know if it was a random choice
and/or if the previous attempted transitions were vetoed. That might allow
the state machine to know if the current input was "expected" or is a
"suprise".

o Write a production system class using the CMM input/callback model.

o Write a neural network class using the CMM input/callback model.

o The subroutine call transitions, action veto of a transition, action forced
state change and multiple matching transitions could be used to implement
"goals" with "backtracking" (like MicroPlanner).


Notes:

o There is error checking on the expressions in transition match lists, but
no error reporting is currently being done.



Usage Hints:

Compiler Warnings: The VisualStudio compiler has problems with the identifier
names created by certain STL templates. (including map<> which is used in the
helper macros). To disable the resulting unreadable warnings, place the
following statement at the top of the file that uses the helper macros:
#pragma warning( disable : 4786 ) The spacing is important so copy it exactly
as is, and paste it starting in column 1.

Input Queue: Actions can call AddInput() to queue up another input message to
be executed when the current input is completed. These queued messages will
be executed before the CSM returns from the original call to Input().

Transition Veto: Actions can test the current situation and return a status
of CSM::STATUS_VETO to tell the CSM to stop processing the current transition
and try to find another one.

Subroutine Call: Use the CSMCallTransition class to specify a transition that
is calling a subroutine in the state diagram. This class allows the return-to
state to be specified (doesn't have to be the current state). The subroutine
must use an CSMReturnTransition class to cause the subroutine to go to the
return-to state.

Multiple Matching Transitions: Use veto actions to test for conditions and
control which transition is taken. Each matching transition will be tried in
turn until one of them is not vetoed by an action.

Random Transition Weights: The weight factors on random transitions are
integers from 0 (zero) to 100 inclusive. These weights are used to calculate
the probability of taking a given transition amongst all the matching random
transitions in a given list. The probabilities are calculated by totaling the
weights of the matching transitions in a give list and dividing each
transition's weight by that total. This allows freedom in the choice of
weight factors. The developer can choose a total weight, then distribute that
amongst the transitions. Or choose the individual weights so that some
transitions execute twice as often as others.
*/
